let x = 123;

Menu.add('main', [
  Menu.element.Text({
    'default': _ => 'Hello',
    'ua': _ => 'Привіт'
  }),
  Menu.element.text({
    'default': _ => `x = ${x}`
  }),
  Menu.element.Button('btnTest', {
    'default': _ => 'clickme'
  }, {
    'click': _ => Menu.load('other')
  }),
]);

Menu.add('other', [
  Menu.element.input('inpTest', 'text', {
    'change': e => console.log(e.value)
  }),
  Menu.element.button('btnBack', {
    'default': _ => 'Return!'
  }, {
    'click': _ => Menu.load('main')
  }),
]);

Menu.load('main');
