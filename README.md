# Menu JS

The purpose of this framework is to simplify the process of implementing a menu in a small browser game.
It was made for team jams where it allows to not waste precious time and get a rather good-looking result.

### Features

* Much less code to write
* Multiple scenes handling
* Language switching
* Common components styling

## Usage

The framework was intended to be used on top of another drawing framework or plain canvas.
Use absolute positioning to place your menu on top of your game creating the illusion of it being drawn
on the canvas while possessing all the power of CSS.

