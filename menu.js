const Menu = new (function () {
  this.language = 'en';
  let scene = null;
  let scenes = {};
  const menu = document.getElementById('menu-js');
  if (!menu) {
    throw new Error('Please, create a div#menu-js.');
  }

  const MenuBuilder = {
    html: ({captions}) =>
      this.language in captions
        ? captions[this.language]()
        : captions.default(),

    button: ({id, captions}) =>
    `<div id="${id}" class="menu-js__button">${
      this.language in captions
        ? captions[this.language]()
        : captions.default()
    }</div>`,
    Button: ({id, captions}) =>
    `<div id="${id}" class="menu-js__Button">${
      this.language in captions
        ? captions[this.language]()
        : captions.default()
    }</div>`,
    input: ({id, inputType}) => `<input id="${id}" type="${inputType}"/>`,
    BUTTON: ({id, captions}) =>
    `<div id="${id}" class="menu-js__BUTTON">${
      this.language in captions
        ? captions[this.language]()
        : captions.default()
    }</div>`,
    text: ({captions}) =>
    `<div class="menu-js__text">${
      this.language in captions
        ? captions[this.language]()
        : captions.default()
    }</div>`,
    Text: ({captions}) =>
    `<div class="menu-js__Text">${
      this.language in captions
        ? captions[this.language]()
        : captions.default()
    }</div>`,
    TEXT: ({captions}) =>
    `<div class="menu-js__TEXT">${
      this.language in captions
        ? captions[this.language]()
        : captions.default()
    }</div>`,
  };
  this.element = {
    // TODO: Update element generation
    html: captions => {return {type: 'html', captions};},

    button: (id, captions, events) => {
      return {type: 'button', id, captions, events};
    },
    Button: (id, captions, events) => {
      return {type: 'Button', id, captions, events};
    },
    input: (id, inputType, events) => {
      return {type: 'input', id, inputType, events};
    },
    BUTTON: (id, captions, events) => {
      return {type: 'BUTTON', id, captions, events};
    },
    text: captions => {return {type: 'text', captions};},
    Text: captions => {return {type: 'Text', captions};},
    TEXT: captions => {return {type: 'TEXT', captions};},
  };

  this.add = (title, elements) => {scenes[title] = elements;};
  this.load = function(title) {
    if (!(title in scenes)) {
      throw new Error(`Scene ${title} is not defined yet.`);
    }
    scene = title;
    let html = '';
    for (let element of scenes[title]) {
      html += MenuBuilder[element.type](element);
    }
    menu.innerHTML = html;
    for (let element of scenes[title]) {
      if (element.id && element.events) {
        const el = document.getElementById(element.id);
        for (let ev in element.events) {
          el.addEventListener(ev, _ => element.events[ev](el));
        }
      }
    }
  };
  this.reload = _ => this.load(scene);

  this.hide = _ => menu.hidden = true;
  this.show = _ => menu.hidden = false;
  this.toggle = _ => menu.hidden = !menu.hidden;
})();
